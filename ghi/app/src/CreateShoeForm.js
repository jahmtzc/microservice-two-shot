import React, {useEffect, useState } from 'react';

function CreateShoeForm() {

  const [bin, setBin] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [modelName, setModelName] = useState('');
  const [bins, setBins] = useState([]);
  const [color, setColor] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');


  const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.bin = bin;
    data.model_name = modelName;
    data.color = color;
    data.picture_url = pictureUrl;
    data.manufacturer=manufacturer;

    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const shoeResponse = await fetch(shoeUrl, fetchOptions);
    if (shoeResponse.ok) {
      setModelName('');
      setPictureUrl('');
      setManufacturer('');
      setColor('');
      setBin('');
    }
  }

  const handleChangeManufacturer = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const handleChangeModelName = (event) => {
    const value = event.target.value;
    setModelName(value);
  }

  const handleChangeColor = (event) => {
    const value = event.target.value;
    setColor(value);
  }
  const handleChangePictureUrl = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  }
  const handleChangeBin = (event) => {
    const value = event.target.value;
    setBin(value);
  }
  let dropdownClasses = 'form-select';
  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';

  return (
              <div>
              <form className={formClasses} onSubmit={handleSubmit} id="create-shoe-form">
                <h1 className="card-title">Create a New Shoe!</h1>

                <p className="mb-3">
                  Now, tell us about your shoe.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeModelName} required placeholder="Shoe Model Name" type="text" id="modelName" name="modelName" value ={modelName} className="form-control" />
                      <label htmlFor="modelName">Model Name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeColor} required placeholder="Shoe Color" type="text" id="color" name="color" value={color} className="form-control" />
                      <label htmlFor="color">Color</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeManufacturer} required placeholder="Shoe Manufacturer" type="text" id="manufacturer" name="manufacturer" value ={manufacturer} className="form-control" />
                      <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangePictureUrl} required placeholder="Picture URL" type="text" id="pictureUrl" name="pictureUrl" value={pictureUrl} className="form-control" />
                      <label htmlFor="pictureUrl">Picture URL</label>
                  <div className="col">
                      <div className="mb-3">
                  <select onChange={handleChangeBin} value={bin} name="bin" id="bin" className={dropdownClasses} required>
                    <option value="">Choose a Bin</option>
                    {bins.map(bin => {
                      return (
                        <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                      )
                    })}
                  </select>
                </div>
                </div>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create a Shoe!</button>
              </form>
              <div className={messageClasses} id="success-message">
                Congratulations! You created a shoe!
              </div>
              </div>
  );
}

export default CreateShoeForm;
